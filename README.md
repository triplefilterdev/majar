# Majar

Platformer with mad jar. Made on [Godot Engine v4.2.1](https://godotengine.org/)

## How to play?

Majar is a cute game where you control a jar. Complete 6 not very exciting levels. Get nothing. Each level is unique and chosen randomly, keep this in mind! The game can be completed in 20 minutes.

You can play the game on [itch.io](https://triplefilterdev.itch.io/majar).

## Game controls

 - "A" and "D" (or arrows) to control the character
 - "M" to mute
 - "R" to level reload 

## Open in editor

Download sources. Download standard Godot 4.5.1 editor. Unarchive project to any folder. Open this folder with Godot.

## Developer

TripleFilterDev
