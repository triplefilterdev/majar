extends Label

@onready var _player_blink: AnimationPlayer = $PlayerBlink

func _ready():
	_player_blink.animation_finished.connect(_on_animation_finished)
	_player_blink.play("blink")

func _on_animation_finished(anim_name: String):
	if anim_name == "blink":
		_player_blink.play("blink")
