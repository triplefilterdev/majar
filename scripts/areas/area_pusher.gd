extends "res://scripts/areas/area_base.gd"

enum ForceDirections
{
	NONE,
	LEFT,
	RIGHT,
	UP,
	DOWN
}

@export_category("Area Pusher")
@export var push_direction: ForceDirections = ForceDirections.NONE

var scroll_speed: float = 1.25

func _ready() -> void:
	super()
	
	_set_shader_params(_dir_to_vector(push_direction))

func _entered(character) -> void:
	character.force_direction = _dir_to_vector(push_direction)

func _dir_to_vector(direction: ForceDirections) -> Vector2i:
	match direction:
		ForceDirections.LEFT: return Vector2i.LEFT
		ForceDirections.RIGHT: return Vector2i.RIGHT
		ForceDirections.UP: return Vector2i.UP * 4
		ForceDirections.DOWN: return Vector2i.DOWN
		
	return Vector2i.ZERO
	
func _set_shader_params(vectori: Vector2) -> void:
	var vector = Vector2(vectori)
	var sprite_material = $Sprite.material
	(sprite_material as ShaderMaterial).set_shader_parameter("speed_horizontal", -vector.normalized().x * scroll_speed)
	(sprite_material as ShaderMaterial).set_shader_parameter("speed_vertical", -vector.normalized().y * scroll_speed)
