extends "res://scripts/areas/area_base.gd"

func _entered(character):
	character.disable_control()
	if not character.marked():
		GlobalLevelManager.level_failed()
	character.mark_lose()
