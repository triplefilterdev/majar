extends Label

func _ready():
	var num_completed_levels = GlobalGameState.state.get_all_completed_levels().size()
	var num_all_levels = GlobalGameState.state.get_levels_count()
	text = "Completed {0}/{1}".format([num_completed_levels, num_all_levels])
