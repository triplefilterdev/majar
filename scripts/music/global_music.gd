extends Node2D

var muted: bool = false

func _ready():
	await get_tree().create_timer(0.65).timeout
	$AudioPlayer.playing = true
	_update_muted()
	
func _process(_delta):
	if Input.is_action_just_pressed("mute_music"):
		muted = not muted
		_update_muted()
	
func _update_muted():
	if muted:
		$AudioPlayer.volume_db = -100
	else:
		$AudioPlayer.volume_db = -15
