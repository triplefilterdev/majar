extends Node2D

@onready var _player_transition: AnimationPlayer = $PlayerTransition
@onready var _wait_scene_changing: bool = false
@onready var _in_transition: bool = false
@onready var _next_scene_path: String = "res://"

func transit_to_scene(path: String):
	_next_scene_path = path
	
	if _in_transition:
		_in_transition = false
		_player_transition.stop()
	_player_transition.play("transition")
	
	_wait_scene_changing = true
	_in_transition = true

func _ready():
	_player_transition.animation_finished.connect(_on_animation_finished)
	_player_transition.play("transition_ready")
	
func _on_animation_finished(anim_name: String):
	if anim_name == "transition" and _wait_scene_changing:
		get_tree().call_deferred("change_scene_to_file", _next_scene_path)
		_wait_scene_changing = false
		_player_transition.play_backwards("transition")
	elif anim_name == "transition" and not _wait_scene_changing:
		_in_transition = false
