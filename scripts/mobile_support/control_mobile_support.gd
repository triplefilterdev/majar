extends Control

@onready var _screen_size = get_viewport().size
@onready var _screen_center = _screen_size / 2
@onready var _touch_helper = $NodeTouchHelper

func _ready():
	#$ButtonLeft.pressed.connect(_on_button_left_pressed)
	#$ButtonRight.pressed.connect(_on_button_right_pressed)
	$ButtonMute.pressed.connect(_on_button_mute_pressed)
	$ButtonRestart.pressed.connect(_on_button_restart_pressed)
	

	

func _on_button_left_pressed():
	Input.action_press("push_left")
	#await get_tree().physics_frame
	Input.action_release("push_left")
	
func _on_button_right_pressed():
	Input.action_press("push_right")
	#await get_tree().physics_frame
	Input.action_release("push_right")
	
func _physics_process(_delta):
	if _touch_helper.state != {}:
		print(_touch_helper.state)
	pass
	#Input.action_release("push_left")
	#Input.action_release("push_right")
	#
	#if $ButtonLeft.button_pressed:
		#Input.action_press("push_left")
		#
	#if $ButtonRight.button_pressed:
		#Input.action_press("push_right")
		#
	#$ButtonLeft.button_pressed = false
	#$ButtonRight.button_pressed = false
			
func _on_button_mute_pressed():
	Input.action_press("mute_music")
	#await get_tree().physics_frame
	Input.action_release("mute_music")
	
func _on_button_restart_pressed():
	Input.action_press("change_level")
	#await get_tree().physics_frame
	Input.action_release("change_level")
