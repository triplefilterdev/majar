extends RigidBody2D

enum PushSide
{
	NONE =  0,
	LEFT = -1,
	RIGHT = 1
}

const PUSH_POWER: float = 70.0
const FORCE_PUSHERS_POWER: float = 120.0
const LEFT_PUSH_OFFSET = Vector2(-5, 5)
const RIGHT_PUSH_OFFSET = Vector2(5, 5)

@onready var force_direction: Vector2i = Vector2i.ZERO : 
	set(value):
		_update_constant_force_direction(value)
	get:
		return constant_force.normalized()
		
@onready var marked_win: bool = false : 
	get:
		return marked_win
		
@onready var marked_lose: bool = false : 
	get:
		return marked_lose
		
@onready var _control_disabled: bool = false
		
func disable_control():
	_control_disabled = true
	
func mark_win():
	marked_win = true
	
func mark_lose():
	marked_lose = true
	
func marked():
	return marked_win || marked_lose
	
func _physics_process(_delta):
	if _control_disabled: return
	
	if Input.is_action_just_pressed("push_left"):
		_push_self(PushSide.LEFT)
	if Input.is_action_just_pressed("push_right"):
		_push_self(PushSide.RIGHT)
		
	if Input.is_action_just_pressed("change_level"):
		GlobalLevelManager.level_failed()

func _push_self(side: PushSide):
	var push_offset
	match side:
		PushSide.LEFT: push_offset = LEFT_PUSH_OFFSET
		PushSide.RIGHT: push_offset = RIGHT_PUSH_OFFSET
		
	apply_impulse((Vector2.UP + Vector2.RIGHT * 0.35 * side) * PUSH_POWER, push_offset)
	
func _update_constant_force_direction(direction: Vector2):
	constant_force = direction * FORCE_PUSHERS_POWER
