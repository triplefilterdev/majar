class_name GameState

extends Object

var _state: Dictionary = {}

func _init():
	make_default()
	
func make_default():
	_state = {
		"in_tutorial": false,
		"levels": {
				"pipedown": { "id": 0, "path": "res://scenes/levels/level_pipedown.tscn", "completed": false },
				"fbird": { "id": 1, "path": "res://scenes/levels/level_fbird.tscn", "completed": false },
				"snake": { "id": 2, "path": "res://scenes/levels/level_snake.tscn", "completed": false },
				"trollhole": { "id": 3, "path": "res://scenes/levels/level_trollhole.tscn", "completed": false },
				"nabokos": { "id": 4, "path": "res://scenes/levels/level_nabokos.tscn", "completed": false },
				"recycle": { "id": 5, "path": "res://scenes/levels/level_recycle.tscn", "completed": false }
			}
		}
func in_tutorial():
	return _state["in_tutorial"]
		
func set_tutorial_completed():
	_state["in_tutorial"] = false

func set_level_completed(id):
	for lvl in _state["levels"]:
		if _state["levels"][lvl]["id"] == id:
			_state["levels"][lvl]["completed"] = true
			print(_state["levels"][lvl])
			return
	
func set_level_uncompleted(id):
	for lvl in _state["levels"]:
		if _state["levels"][lvl]["id"] == id:
			_state["levels"][lvl]["completed"] = false
			
func get_path_by_id(id):
	for lvl in _state["levels"]:
		if _state["levels"][lvl]["id"] == id:
			print(_state["levels"][lvl]["path"])
			return _state["levels"][lvl]["path"]
	
func get_all_completed_levels():
	var ids = []
	
	for lvl in _state["levels"]:
		if _state["levels"][lvl]["completed"]:
			ids.append(_state["levels"][lvl]["id"])
			
	return ids
	
func get_all_completed_levels_paths():
	var paths = []
	
	for lvl in _state["levels"]:
		if _state["levels"][lvl]["completed"]:
			paths.append(_state["levels"][lvl]["path"])
			
	return paths
	
func get_all_uncompleted_levels():
	var ids = []
	
	for lvl in _state["levels"]:
		if not _state["levels"][lvl]["completed"]:
			print("lvl ", lvl, " completed ", _state["levels"][lvl]["completed"])
			ids.append(_state["levels"][lvl]["id"])
			
	print(ids)
			
	return ids
	
func get_all_uncompleted_levels_paths():
	var paths = []
	
	for lvl in _state["levels"]:
		if not _state["levels"][lvl]["completed"]:
			print("lvl ", lvl, " completed ", _state["levels"][lvl]["completed"])
			paths.append(_state["levels"][lvl]["path"])
			
	return paths
	
func get_levels_count():
	return _state["levels"].size()

func is_empty():
	return _state == {}
