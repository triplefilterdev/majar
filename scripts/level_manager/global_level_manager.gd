extends Node2D

const WIN_SCENE_PATH: String = "res://scenes/interface/menu_end.tscn"

@onready var _current_level_id: int = -1

func level_failed():
	if GlobalGameState.state.in_tutorial():
		_change_scene_to_current()
		return
		
	_check_and_continue()
	
func level_completed():
	if GlobalGameState.state.in_tutorial():
		_check_and_continue()
		return
		
	GlobalGameState.state.set_level_completed(_current_level_id)
	_check_and_continue()
	
func _check_and_continue():
	if GlobalGameState.state.get_all_uncompleted_levels().size() == 0:
		_end_game()
		return
	
	_current_level_id = _find_random_uncompleted_level()
	_change_scene_to_current()
	
func _find_random_uncompleted_level():
	randomize()
	
	var levels = GlobalGameState.state.get_all_uncompleted_levels()
	if levels.size() == 0: return -1
	
	print(levels.size())
	var random_level_id = levels[randi() % levels.size()]
	return random_level_id
	
func _end_game():
	_change_scene(WIN_SCENE_PATH)
	
func _change_scene_to_current():
	_change_scene(_get_level_path(_current_level_id))
	
func _change_scene(path):
	#get_tree().call_deferred("change_scene_to_file", path)
	GlobalTransition.transit_to_scene(path)
	
func _get_level_path(id):
	return GlobalGameState.state.get_path_by_id(id)
